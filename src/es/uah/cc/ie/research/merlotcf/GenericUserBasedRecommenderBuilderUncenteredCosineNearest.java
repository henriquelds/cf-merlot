/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package es.uah.cc.ie.research.merlotcf;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
//import org.apache.mahout.cf.taste.impl.similarity.UncenteredCosineSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

/**
 *
 * @author Cristian Cechinel
 */
public final class GenericUserBasedRecommenderBuilderUncenteredCosineNearest implements RecommenderBuilder {

    private int _neighborhoodSize;
    private double _minSimilarity;

    public GenericUserBasedRecommenderBuilderUncenteredCosineNearest(int neigborhoodSize,
                                            double minSimilarity){
        _neighborhoodSize = neigborhoodSize;
        _minSimilarity = minSimilarity;
    }
  @Override
  public Recommender buildRecommender(DataModel dataModel) throws TasteException  {
    UserSimilarity userSimilarity = new PearsonCorrelationSimilarity(dataModel);

    UserNeighborhood neighborhood =
          new NearestNUserNeighborhood(_neighborhoodSize, _minSimilarity, userSimilarity, dataModel, 1);

    //System.out.println(dataModel.getNumUsers());
    return new GenericUserBasedRecommender(dataModel,
                                           neighborhood,
                                           userSimilarity);
  }

}
