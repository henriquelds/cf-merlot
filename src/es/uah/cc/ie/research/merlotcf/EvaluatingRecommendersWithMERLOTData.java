package es.uah.cc.ie.research.merlotcf;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.EuclideanDistanceSimilarity;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.apache.mahout.cf.taste.eval.IRStatistics;




/**
 *
 * @author Miguel-Angel Sicilia
 */
public class EvaluatingRecommendersWithMERLOTData {


    public static void main(String args[]) throws TasteException, IOException{



        //(pathToDataSet, training, eval, neighborhoodSize, minSimilarity)
       
        /*
        RecommenderEvaluatorUtil eval = new RecommenderEvaluatorUtil(
                   "C:\\CollaborativeFiltering\\cf-merlot\\data.txt", 0.9, 0.3, 10, 0.8);
*/

        //System.out.println(" ----- LOGLIKELIHOOD -------");
        //repeatedEvalLogLikelihood();
        
        //System.out.println(" ----- PEARSON -------");
        //repeatedEvalPearsonUserBased();
        //repeatedEvalPearsonItemBased();

        //System.out.println(" ----- EUCLIDEAN -------");
        repeatedEvalEuclidean();
        /*

        System.out.println(" ----- LOGLIKELIHOOD -------");
        repeatedEvalLogLikelihood();

        System.out.println(" ----- COSINE -------");
        repeatedEvalUncenteredCosine();


        System.out.println(" ----- SPEARMAN -------");
        repeatedEvalSpearman();*/


/*
        UserSimilarity userSimilarity = n   new NearestNUserNeighborhood(10, userSimilarity, eval.getDataModel());
         Recommender rec =  new Generew EuclideanDistanceSimilarity(eval.getDataModel());
        UserNeighborhood neighborhood =
          new NearestNUserNeighborhood(10, userSimilarity, eval.getDataModel());
         Recommender rec =  new GenericUserBasedRecommender(eval.getDataModel(),
                                           neighborhood,
                                           userSimilarity);
        eval.generateRecommendations( rec, "C:\\CollaborativeFiltering\\cf-merlot\\out2.txt");

                /* e estimatePreference().
        try {
            cachingRecommender = new CachingRecommender(recommender);
        } catch (TasteException ex)     {
            Logger.getLogger(EvaluatingRecommendersWithMERLOTData.class.getName()).log(Level.SEVERE, null, ex);
        }
        List<RecommendedItem> recommendations= null;
        try {
            int count =0;
            for (int i=0; i < Integer.MAX_VALUE; i++){
            recommendations = cachingRecommender.recommend(i, 10);
              if (recommendations.size() >0) 
                  System.out.println(i+"->"+recommendations); count++;
            }
            System.out.println("count="+count);
        } catch (TasteException ex) {
            Logger.getLogger(EvaluatingRecommendersWithMERLOTData.class.getName()).log(Level.SEVERE, null, ex);
        }
              */
    }

    public static void repeatedEvalPearsonUserBased() throws TasteException, IOException{
        String dir = "E:\\bolsa-cf\\work\\los_desc_total\\";
        String[] arr = {"newData"};
        
        int cont;  
        for(cont = 0; cont < arr.length; cont++){
            BufferedWriter bw = new BufferedWriter(new FileWriter(dir+"pearson\\evaluation-pearson-nearest-"+arr[cont]+".txt"));
            bw.write("neighborhood, similarity, average, rms");
            bw.write("\n");
            bw.flush();

                // Evaluate Pearson algorithm:
                // For different neighborhood sizes:
            for (int i=2; i <=20; i++){
                // For different minimum similarity:
                for (double j=0.1; j<=0.9; j+=0.1){
                    RecommenderEvaluatorUtil eval = new RecommenderEvaluatorUtil(
                       dir+arr[cont]+".txt", 0.9, 1, i, j);
                    double accPearson = 0; double acc=0;
                    double accPearson2 = 0; double acc2=0;

                  // Evaluate repeteadly
                    for (int k = 0;k < 50; k++){
                        double p =  eval.getAverageAbsoluteDifferenceEvaluationPearsonUserBased();
                        if (!new Double(p).equals(Double.NaN)) {
                            accPearson += p;
                            acc ++;
                        }
                        double p2 =  eval.getRMSEvaluationNearest();
                        if (!new Double(p2).equals(Double.NaN)) {
                            accPearson2 += p2;
                            acc2 ++;
                        }
                    }
                    //System.out.println("LogLikelihood para :"+ i +"vecinos="+accLogLikelihood/acc);
                    bw.write(i+","+j+","+accPearson/acc+","+accPearson2/acc2); bw.write("\n");
                    bw.flush();
                }
            }
        }
        /*  BufferedWriter bw = new BufferedWriter(new FileWriter("evaluation-pearson-nearest-UB-newData.txt"));
          bw.write("neighborhood, similarity, average, rms");
          bw.write("\n");
          bw.flush();
          
         // Evaluate Pearson algorithm:
          // For different neighborhood sizes: i stands for neighborhoodSize
        for (int i=1; i <=10; i++)
            // For different minimum similarity:
            for (double j=0.1; j<=1; j+=0.1){ //j stands for minSimilarity
              //(pathToDataSet, training, eval, neighborhoodSize, minSimilarity)
              RecommenderEvaluatorUtil eval = new RecommenderEvaluatorUtil(
                   "newData.txt", 0.9, 1, i, j);
              double accPearson = 0; double acc=0;
              double accPearson2 = 0; double acc2=0;
              // Evaluate repeteadly
              for (int k = 0;k < 50; k++){//k stands for the number of interactions to get an average error
                  double p =  eval.getAverageAbsoluteDifferenceEvaluationPearsonUserBased();
                  if (!new Double(p).equals(Double.NaN)) {
                      accPearson += p;
                      acc ++;
                  }
                  double p2 =  eval.getRMSEvaluationNearest();
                  if (!new Double(p2).equals(Double.NaN)) {
                      accPearson2 += p2;
                      acc2 ++;
              }
            }
        System.out.println("Pearson para :"+ i +"vecinos="+accPearson/acc);
        bw.write(i+","+j+","+accPearson/acc+","+accPearson2/acc2); bw.write("\n");
        bw.flush();
        
        }*/

 }
    public static void repeatedEvalPearsonItemBased() throws TasteException, IOException{

          BufferedWriter bw = new BufferedWriter(new FileWriter("evaluation-pearson-nearest-IB.txt"));
          bw.write("similarity, average, rms");
          bw.write("\n");
          bw.flush();
          
         // Evaluate Pearson algorithm:
            // For different minimum similarity:
            for (double j=0.1; j<=1; j+=0.1){ //j stands for minSimilarity
              //(pathToDataSet, training, eval, minSimilarity)
              RecommenderEvaluatorUtil eval = new RecommenderEvaluatorUtil(
                   "decemberData.txt", 0.9, 1, j);
              double accPearson = 0; double acc=0;
              double accPearson2 = 0; double acc2=0;
              // Evaluate repeteadly
              for (int k = 0;k < 1; k++){//k stands for the number of interactions to get an average error
                  double p =  eval.getAverageAbsoluteDifferenceEvaluationPearsonItemBased();
                  if (!new Double(p).equals(Double.NaN)) {
                      accPearson += p;
                      acc ++;
                  }
                  double p2 =  eval.getRMSEvaluationNearest();
                  if (!new Double(p2).equals(Double.NaN)) {
                      accPearson2 += p2;
                      acc2 ++;
              }
            }
        System.out.println("Pearson para minSimilarity:"+j+":"+accPearson/acc);
        bw.write(""+j+","+accPearson/acc+","+accPearson2/acc2); bw.write("\n");
        bw.flush();
        
        }

 }

     public static void repeatedEvalEuclidean() throws TasteException, IOException{
         String dir = "E:\\bolsa-cf\\work\\los_desc_kmeans_total\\";
        //String[] arr = {"5651", "5661", "5668", "5669","5676", "5677", "5679", "5680", "5682", "5684", "5685"};
        ArrayList<String> array = getClustersFilenames(dir);
        
        int cont;  
        for(cont = 0; cont < array.size(); cont++){
            BufferedWriter bw = new BufferedWriter(new FileWriter(dir+"euclidean\\60-40_evaluation-euclidean-nearest-"+array.get(cont)));
            bw.write("neighborhood, similarity, average, rms");
            bw.write("\n");
            bw.flush();

                // Evaluate Pearson algorithm:
                // For different neighborhood sizes:
            for (int i=2; i <=20; i++){
                // For different minimum similarity:
                for (double j=0.1; j<=0.9; j+=0.1){
                    RecommenderEvaluatorUtil eval = new RecommenderEvaluatorUtil(
                       dir+array.get(cont), 0.6, 1, i, j);        //alterada porcentagem treino-teste pra 60-40
                    double accEuclidean = 0; double acc=0;
                    double accEuclidean2 = 0; double acc2=0;

                  // Evaluate repeteadly
                    for (int k = 0;k < 50; k++){
                        double p =  eval.getAverageAbsoluteDifferenceEvaluationEuclidean();
                        if (!new Double(p).equals(Double.NaN)) {
                            accEuclidean += p;
                            acc ++;
                        }
                        double p2 =  eval.getRMSEvaluationNearest();
                        if (!new Double(p2).equals(Double.NaN)) {
                            accEuclidean2 += p2;
                            acc2 ++;
                        }
                    }
                    //System.out.println("LogLikelihood para :"+ i +"vecinos="+accLogLikelihood/acc);
                    bw.write(i+","+j+","+accEuclidean/acc+","+accEuclidean2/acc2); bw.write("\n");
                    bw.flush();
                }
            }
        }
         
         /*
          BufferedWriter bw = new BufferedWriter(new FileWriter("evaluation-euclidean-nearest.txt"));
          bw.write("neighborhood, similarity, average, rms");
          bw.write("\n");
          bw.flush();

        // Evaluate Pearson algorithm:
        // For different neighborhood sizes:
        for (int i=1; i <=30; i++)
            // For different minimum similarity:
            for (double j=0.1; j<=1; j+=0.1){
              RecommenderEvaluatorUtil eval = new RecommenderEvaluatorUtil(
                   "decemberData.txt", 0.9, 1, i, j);
              double accEuclidean = 0; double acc=0;
              double accEuclidean2 = 0; double acc2=0;

              // Evaluate repeteadly
              for (int k = 0;k < 100; k++){
                  double p =  eval.getAverageAbsoluteDifferenceEvaluationEuclidean();
                  if (!new Double(p).equals(Double.NaN)) {
                  // System.out.println("**************"+p);
                      accEuclidean += p;
                      acc ++;
                 }
                 double p2 =  eval.getRMSEvaluationNearest();
                  if (!new Double(p2).equals(Double.NaN)) {
                      accEuclidean2 += p2;
                      acc2 ++;
              }
        }
        System.out.println("Euclidean para :"+ i +"vecinos="+accEuclidean/acc);
        bw.write(i+","+j+","+accEuclidean/acc+","+accEuclidean2/acc2); bw.write("\n");
        bw.flush();
        }*/
     }

    public static void repeatedEvalLogLikelihood() throws TasteException, IOException{
       String dir = "E:\\bolsa-cf\\work\\los_desc_kmeans_11\\";
        //String[] arr = {"5651", "5661", "5668", "5669","5676", "5677", "5679", "5680", "5682", "5684", "5685"};
        ArrayList<String> array = getClustersFilenames(dir);
        
        int cont;  
        for(cont = 0; cont < array.size(); cont++){
            BufferedWriter bw = new BufferedWriter(new FileWriter(dir+"NEW_evaluation-loglikelihood-nearest-"+array.get(cont)));
            bw.write("neighborhood, similarity, average, rms");
            bw.write("\n");
            bw.flush();

                // Evaluate Pearson algorithm:
                // For different neighborhood sizes:
            for (int i=2; i <=20; i++){
                // For different minimum similarity:
                for (double j=0.1; j<=0.9; j+=0.1){
                    RecommenderEvaluatorUtil eval = new RecommenderEvaluatorUtil(
                       dir+array.get(cont), 0.9, 1, i, j);
                    double accLogLikelihood = 0; double acc=0;
                    double accLogLikelihood2 = 0; double acc2=0;

                  // Evaluate repeteadly
                    for (int k = 0;k < 50; k++){
                        double p =  eval.getAverageAbsoluteDifferenceEvaluationLogLikelihoodNearest();
                        if (!new Double(p).equals(Double.NaN)) {
                            accLogLikelihood += p;
                            acc ++;
                        }
                        double p2 =  eval.getRMSEvaluationNearest();
                        if (!new Double(p2).equals(Double.NaN)) {
                            accLogLikelihood2 += p2;
                            acc2 ++;
                        }
                    }
                    //System.out.println("LogLikelihood para :"+ i +"vecinos="+accLogLikelihood/acc);
                    bw.write(i+","+j+","+accLogLikelihood/acc+","+accLogLikelihood2/acc2); bw.write("\n");
                    bw.flush();
                }
            }
        }
    }
  public static void repeatedEvalSpearman() throws TasteException, IOException{

          BufferedWriter bw = new BufferedWriter(new FileWriter("evaluation-spearman-nearest.txt"));
          bw.write("neighborhood, similarity, average, rms");
          bw.write("\n");
          bw.flush();

        // Evaluate Pearson algorithm:
        // For different neighborhood sizes:
        for (int i=1; i <=30; i++)
            // For different minimum similarity:
            for (double j=0.1; j<=1; j+=0.1){
              RecommenderEvaluatorUtil eval = new RecommenderEvaluatorUtil(
                   "decemberData.txt", 0.9, 1, i, j);
              double accSpearman = 0; double acc=0;
              double accSpearman2 = 0; double acc2=0;

              // Evaluate repeteadly
              for (int k = 0;k < 100; k++){
                  double p =  eval.getAverageAbsoluteDifferenceEvaluationSpearmanNearest();
                  if (!new Double(p).equals(Double.NaN)) {
                  // System.out.println("**************"+p);
                      accSpearman += p;
                      acc ++;
                 }
                 double p2 =  eval.getRMSEvaluationNearest();
                  if (!new Double(p2).equals(Double.NaN)) {
                      accSpearman2 += p2;
                      acc2 ++;
              }
        }
        System.out.println("Spearman para :"+ i +"vecinos="+accSpearman/acc);
        bw.write(i+","+j+","+accSpearman/acc+","+accSpearman2/acc2); bw.write("\n");
        bw.flush();
        }
     }

public static void repeatedEvalUncenteredCosine() throws TasteException, IOException{

          BufferedWriter bw = new BufferedWriter(new FileWriter("evaluation-uncenteredcosine-nearest.txt"));
          bw.write("neighborhood, similarity, average, rms");
          bw.write("\n");
          bw.flush();

          // For different neighborhood sizes:
        for (int i=1; i <=30; i++)
            // For different minimum similarity:
            for (double j=0.1; j<=1; j+=0.1){
              RecommenderEvaluatorUtil eval = new RecommenderEvaluatorUtil(
                   "decemberData.txt", 0.9, 1, i, j);
              double accUncentered = 0; double acc=0;
              double accUncentered2 = 0; double acc2=0;

              // Evaluate repeteadly
              for (int k = 0;k < 100; k++){
                  double p =  eval.getAverageAbsoluteDifferenceEvaluationUncenteredCosineNearest();
                  if (!new Double(p).equals(Double.NaN)) {
                  // System.out.println("**************"+p);
                      accUncentered += p;
                      acc ++;
                 }
                 double p2 =  eval.getRMSEvaluationNearest();
                  if (!new Double(p2).equals(Double.NaN)) {
                      accUncentered2 += p2;
                      acc2 ++;
              }
        }
        System.out.println("Uncentered para :"+ i +"vecinos="+accUncentered/acc);
        bw.write(i+","+j+","+accUncentered/acc+","+accUncentered2/acc2); bw.write("\n");
        bw.flush();
        }
     }

     public static ArrayList<String> getClustersFilenames(String dir){
        File file = new File(dir);  
        File[] files = file.listFiles();
        ArrayList<String> a = new ArrayList<String>();
        for (File f:files)  
        {  
            if(f.getName().matches("[0-9][0-9][0-9][0-9].txt")){
                a.add(f.getName());
            }
             
        }  
        System.out.println(a.size());
        return a;
     }
}