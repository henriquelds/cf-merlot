package es.uah.cc.ie.research.merlotcf;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.eval.AverageAbsoluteDifferenceRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.recommender.CachingRecommender;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;

import org.apache.mahout.cf.taste.eval.RecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.eval.GenericRecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.eval.IRStatistics;
import org.apache.mahout.cf.taste.impl.eval.RMSRecommenderEvaluator;

/**
 * Evaluates several recommender algorithms for the same data set
 * with different parameters.
 *
 * @author Miguel-Angel Sicilia
 */
public class RecommenderEvaluatorUtil {

    private String _dataset;
    private FileDataModel _data;
    private double _training;
    private double _eval;
    private int _neighborhoodSize;
    private double _minSimilarity;
    private RecommenderBuilder _recommenderBuilder;


    public RecommenderEvaluatorUtil(String pathToDataSet, double training, double eval,
            int neighborhoodSize, double minSimilarity ){
       _dataset = pathToDataSet;_training = training; _eval = eval;
       _neighborhoodSize = neighborhoodSize; _minSimilarity= minSimilarity; 
        try {
             _data = new FileDataModel(new File(_dataset));
        } catch (IOException ex) {
            Logger.getLogger(
                    EvaluatingRecommendersWithMERLOTData.class.getName()).log(
                                                        Level.SEVERE, null, ex);
        }
    }
    
    public RecommenderEvaluatorUtil(String pathToDataSet, double training, double eval,
            double minSimilarity ){
       _dataset = pathToDataSet;_training = training; _eval = eval;
       _minSimilarity= minSimilarity; 
        try {
             _data = new FileDataModel(new File(_dataset));
        } catch (IOException ex) {
            Logger.getLogger(
                    EvaluatingRecommendersWithMERLOTData.class.getName()).log(
                                                        Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @return The average absolute difference.
     */

 

    public double getAverageAbsoluteDifferenceEvaluationPearsonUserBased() throws TasteException{
        AverageAbsoluteDifferenceRecommenderEvaluator eval =
                new AverageAbsoluteDifferenceRecommenderEvaluator();

        _recommenderBuilder = new GenericUserBasedRecommenderBuilderPearson(_neighborhoodSize,
                                                _minSimilarity);
        return eval.evaluate( _recommenderBuilder,
                              null,
                              _data,
                              _training,
                              _eval);
    }
    
    public double getAverageAbsoluteDifferenceEvaluationPearsonItemBased() throws TasteException{
        AverageAbsoluteDifferenceRecommenderEvaluator eval =
                new AverageAbsoluteDifferenceRecommenderEvaluator();

        _recommenderBuilder = new GenericItemBasedRecommenderBuilderPearson(_minSimilarity);
        return eval.evaluate( _recommenderBuilder,
                              null,
                              _data,
                              _training,
                              _eval);
    }
    /**
     *
     * @return The average absolute difference.
     */
    public double getAverageAbsoluteDifferenceEvaluationEuclidean() throws TasteException{
        AverageAbsoluteDifferenceRecommenderEvaluator eval =
                new AverageAbsoluteDifferenceRecommenderEvaluator();

        _recommenderBuilder = new GenericUserBasedRecommenderBuilderEuclidean(_neighborhoodSize,
                              _minSimilarity);
        return eval.evaluate(_recommenderBuilder,
                              null,
                              _data,
                              _training,
                              _eval);
    }

       public double getAverageAbsoluteDifferenceEvaluationLogLikelihoodNearest() throws TasteException{
        AverageAbsoluteDifferenceRecommenderEvaluator eval =
                new AverageAbsoluteDifferenceRecommenderEvaluator();

        _recommenderBuilder = new GenericUserBasedRecommenderBuilderLogLikelihoodNearest(_neighborhoodSize,
                              _minSimilarity);

        return eval.evaluate( _recommenderBuilder,
                              null,
                              _data,
                              _training,
                              _eval);
    }

     public double getAverageAbsoluteDifferenceEvaluationSpearmanNearest() throws TasteException{
        AverageAbsoluteDifferenceRecommenderEvaluator eval =
                new AverageAbsoluteDifferenceRecommenderEvaluator();

        _recommenderBuilder = new GenericUserBasedRecommenderBuilderSpearmanNearest(_neighborhoodSize,
                              _minSimilarity);

        return eval.evaluate( _recommenderBuilder,
                              null,
                              _data,
                              _training,
                              _eval);
    }

     public double getAverageAbsoluteDifferenceEvaluationUncenteredCosineNearest() throws TasteException{
        AverageAbsoluteDifferenceRecommenderEvaluator eval =
                new AverageAbsoluteDifferenceRecommenderEvaluator();

        _recommenderBuilder = new GenericUserBasedRecommenderBuilderUncenteredCosineNearest(_neighborhoodSize,
                              _minSimilarity);

        return eval.evaluate( _recommenderBuilder,
                              null,
                              _data,
                              _training,
                              _eval);
    }

// it must be executed after AverageAbsoluteDifference
    public double getRMSEvaluationNearest() throws TasteException {
        RMSRecommenderEvaluator eval =
               new RMSRecommenderEvaluator();
        return eval.evaluate( _recommenderBuilder,
                              null,
                              _data,
                              _training,
                              _eval);
        }

    public void generateRecommendations(Recommender rec, String outFile) throws IOException{

        // Create output stream:
       BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
       // Create recommender:
       CachingRecommender cachingRecommender = null;
          try {
            cachingRecommender = new CachingRecommender(rec);
        } catch (TasteException ex) {
            Logger.getLogger(EvaluatingRecommendersWithMERLOTData.class.getName()).log(Level.SEVERE, null, ex);
        }
       // Get recommendations and iterate
        List<RecommendedItem> recommendations= null;
        try {
            int count =0;
            // For each user (max user id found was 400000):
            for (int i=0; i < 400/*Integer.MAX_VALUE*/; i++){
                System.out.println(i);
            recommendations = cachingRecommender.recommend(i, 10);
              if (recommendations.size() >0){
                  System.out.println("**");
                  Iterator it = recommendations.iterator(); count=0;
                  while (it.hasNext()){
                  //System.out.println(i+"->"+recommendations);
                      RecommendedItem ri = (RecommendedItem)it.next();
                      count++;
                      bw.write(i +"#"+ ri.getItemID() +"#"+ri.getValue()+"#"+count);
                      bw.write("\n");
                      bw.flush();
                  }
              }
            }
            System.out.println("count="+count);
        } catch (TasteException ex) {
            Logger.getLogger(EvaluatingRecommendersWithMERLOTData.class.getName()).log(Level.SEVERE, null, ex);
        }



    }

    public DataModel getDataModel(){
        return _data;
    }
}
