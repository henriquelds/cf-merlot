/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.uah.cc.ie.research.merlotcf;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;

/**
 *
 * @author henrique
 */
public class GenericItemBasedRecommenderBuilderPearson implements RecommenderBuilder {
    private double minSimilarity;
    
    public GenericItemBasedRecommenderBuilderPearson(double minSimilarity) {
        this.minSimilarity = minSimilarity;
    }

    public Recommender buildRecommender(DataModel dm) throws TasteException {
        ItemSimilarity itemSimilarity = new PearsonCorrelationSimilarity(dm);
        
        return new GenericItemBasedRecommender(dm,itemSimilarity);
        
    }
    
    
}
